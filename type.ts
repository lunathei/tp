import Orders from "Interface/orders.interface";
import Delivery from "Interface/delivery.interface";
import Package from "Interface/packages.interface";

export default class Order implements Orders {
  id: number;
  packages: Package[];
  delivery: Delivery | null;
  constructor(id: number, packages: Package[], delivery: Delivery | null) {
    this.id = id;
    this.packages = packages;
    this.delivery = delivery;
  }
}