import { Router, Request, Response } from 'express'
import { request } from 'http'
import storage from 'node-persist'
import RouteModel from '../Models/route.model'
import Order from '../type';
import ContactAnonyme from '../Models/contact.model';


export default class RouteController{
    
    send(res: Response): void{
        res.send('Hello World !')
    }

    status(res: Response): void {
        res.status(123);
    }

 
    static async getOrders(req: Request, res: Response){
        let result  : Order[] = [];
        const orders = await RouteModel.findOrders();
        orders.forEach((data : Order) => {
            let tmp = new Order(data.id, data.packages, data.delivery);
            if(tmp.delivery){
                tmp.delivery.contact= new ContactAnonyme();
            }
            result.push(tmp);
        });
     
        res.json({orders:result})

    }

    static async getOrdersById(req: Request, res: Response){
        const id = req.params.id
        const orders = await RouteModel.findOrders();
        const result = orders.find((order: any) => order.id === parseInt(id, 10))

        if (!result) {
            return res.sendStatus(404)
        }
        return res.json(result)
    }

    static async postOrders(req: Request, res: Response){
        const payload = req.body

        const orders = await RouteModel.findOrders();
        const alreadyExists = orders.find((order: any) => order.id === payload.id)

        if (alreadyExists) {
            return res.sendStatus(409)
        }
        
        orders.push(payload)
        await RouteModel.setIt(orders)

        res.sendStatus(201)
    }
}