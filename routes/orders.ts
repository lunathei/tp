import { Router, Request, Response } from 'express'
import storage from 'node-persist'
import RouteController from '../Controllers/route.Controller'
const router = Router()

router.get('/', async (req: Request, res: Response) => {
  return await RouteController.getOrders(req, res)
})

router.get('/:id', async (req: Request, res: Response) => {
  return await RouteController.getOrdersById(req, res)
})

router.post('/', async (req: Request, res: Response) => {
  return await RouteController.postOrders(req, res)
})

export default router