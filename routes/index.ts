import { Router, Request, Response } from 'express'
import RouteController from '../Controllers/route.Controller'
const controller = new RouteController();
const router = Router()

router.get('/', async (req: Request, res: Response) => {
  controller.send(res)
})

router.get('/favicon.ico', (req: Request, res: Response) => controller.status(res))

export default router