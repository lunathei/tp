import http from 'http'
import express from 'express'
import App from './app'


export default class Server {
    public port: number | boolean;
    public app: express.Application;

    constructor(port:number |boolean ){
        this.port = port;
        this.app = new App().getInstance;
    }

    public start(): void{
    
        const port = this.normalizePort(process.env.PORT || 1234)
        this.app.set('port', this.port)
        const server = http.createServer(this.app)
        server.listen(port)
        server.on('listening', () => this.onListening())
        server.on('error', (error) => this.onError(error))


    }

    normalizePort(value: any): number | boolean {
        const port = parseInt(value, 10)
      
        if (isNaN(port)) {
          return value
        }
      
        if (port >= 0) {
          return port
        }
      
        return false
      }

      onListening(): void {
        console.log(`Listening on ${this.port}`)
      }

      onError(error: any): never {
        if (error.syscall !== 'listen') {
          throw error
        }
      
        switch (error.code) {
          case 'EACCES':
            console.error(`${this.port} requires elevated privileges`)
            process.exit(1)
          case 'EADDRINUSE':
            console.error(`${this.port} is already in use`)
            process.exit(1)
          default:
            throw error
        }
      }

}