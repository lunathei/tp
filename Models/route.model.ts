import index from '../routes/index'
import storage from 'node-persist'
import Storage from './storage.model';
import Order from '../type'

export default class RouteModel{

    static async findOrders():Promise<Order[]>{
        const orders =  await Storage.getItem('orders');
        return orders;
    }

    static async setIt(orders: object){
        return Storage.setItem('orders', orders)
    }

    static async findOrderById(id: string): Promise<Order>{
        const orders =  await Storage.getItem('orders');
        const result = orders.find((order: any) => order.id === parseInt(id, 10))
        return result;
    }

    static async postOrder(req: Order) : Promise<boolean>{
        const orders = await this.findOrders();
        const alreadyExists = await this.findOrderById(req.id+"");
        if (alreadyExists) {
         return false;
        }
        orders.push(req)
        await Storage.setItem('orders', orders);
        return true;
    }
    
}

