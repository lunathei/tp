import storage from 'node-persist'
import Order from '../type';
import { orders } from '../data/_orders'


export default class Storage {
  
    constructor(){
       this.initStorage(); 
         
    }

    initStorage() : void {
        storage.init().then(() => {
            let x: Order[] = [] ;
            orders.forEach(data =>{
             x.push(new Order(data.id,data.packages,data.delivery))
            })
            storage.setItem('orders', x)
          })
    }

    static async  getItem(obj : string) : Promise<any>{
        return await storage.getItem(obj)
    }

    static async  setItem(obj : string, object : Object){
        await storage.setItem(obj,object);
    }

}