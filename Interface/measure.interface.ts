export default interface Measure{
    unit: string,
    value: number
}