import Delivery from "./delivery.interface";
import Package from "./packages.interface";

export default interface Orders {
    id : number
    packages:Package[]
    delivery: Delivery | null
    
}