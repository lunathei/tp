export default interface Contact {
    fullname: string;
    email: string;
    phone: string;
    address: string;
    postalCode: string;
    city: string;
  }