import Interval from "./interval.interface";
import Contact from "./contact.interface";
import Carrier from "./carrier.interface";


export default interface Delivery{
    storePickingInterval: Interval
    deliveryInterval: Interval
    contact: Contact
    carrier: Carrier

}