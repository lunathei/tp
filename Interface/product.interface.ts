import Price from "./price.interface";

export default interface Product{
   name: string
   price: Price
}