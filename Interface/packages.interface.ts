import Measure from "./measure.interface";
import Product from "./product.interface";

export default interface Packages{
    length: Measure
    width: Measure
    height: Measure
    weight: Measure
    products: Product[]

}